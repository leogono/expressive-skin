<?php
	
// ThesisLove Widgets developed by Raaj Trambadia //

	
/* ============[[  ThesisLove -> Aweber Optin Form Widget ]]=============== */


/* Registering The Widget */

$widgets = array('thesislove_optin');
foreach ($widgets as $widget)
	register_widget($widget);

class thesislove_optin extends WP_Widget {
	function thesislove_optin() {
		$widget_ops = array('classname' => 'thesislove-optin-form', 'description' => __('Easy Create An Optin Form For Your Aweber List. Wait - you can create as many as you want.', 'thesislove'));
		$control_ops = array('id_base' => 'thesislove_optin');
		$this->WP_Widget('thesislove_optin', __('ThesisLove &#8594; Aweber Optin Form ', 'thesislove'), $widget_ops, $control_ops);
}
	
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$description = $instance['description'];
		$email_input = $instance['email_input'];
		$name_input = $instance['name_input'];
		$button_color = $instance['button_color'];
		$button_text = $instance['button_text'];
		$listname = $instance['listname'];
		$web_tracking = $instance['web_tracking'];
		
/* Widget In The Frontend */
		
			?>
            <? echo $before_widget; ?>
            <? if (!empty($title))
      echo $before_title . $title . $after_title;; ?>
            <div class="form_desc"><p><? echo $description ?></p></div>
            
        <div class="thesislove-optin">
<form method="post" action="http://www.aweber.com/scripts/addlead.pl"  >
<div style="display: none;">
		<input type="hidden" name="meta_web_form_id" value="" /><br />
		<input type="hidden" name="meta_split_id" value="" /><br />
		<input type="hidden" name="listname" value="<? echo $listname ?>" /><br />
		<input type="hidden" name="redirect" value="http://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_8fb78b8c1d56730b21b4d6bb5dd321b3" /><br />
		<input type="hidden" name="meta_adtracking" value="<? echo $web_tracking ?>" /><br />
		<input type="hidden" name="meta_message" value="1" /><br />
		<input type="hidden" name="meta_required" value="name,email" /><br />
		<input type="hidden" name="meta_tooltip" value="" />
	</div>
<p><input type="text" name="name" class="name" placeholder="<? echo $name_input ?>" tabindex="600" /></p>
<p><input class="email" type="email" name="email" placeholder="<? echo $email_input ?>" tabindex="601" /></p>
<p><input class="button <? echo $button_color ?>" name="submit" type="submit" value="<? echo $button_text ?>" tabindex="602" /></p>
</form>
</div>
<? echo $after_widget; ?>
<?
}
	
/* Widget Update Functions */
			
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = sprintf('%s', strip_tags(stripslashes($new_instance['title'])));
		$instance['description'] = $new_instance['description'];
		$instance['email_input'] = sprintf('%s', wp_kses_data($new_instance['email_input']));
		$instance['name_input'] = sprintf('%s', strip_tags(stripslashes($new_instance['name_input'])));
		$instance['button_color'] = $new_instance['button_color'];
		$instance['button_text'] = sprintf('%s', strip_tags(stripslashes($new_instance['button_text'])));
		$instance['listname'] = $new_instance['listname'];
		$instance['web_tracking'] = $new_instance['web_tracking'];
		return $instance;
	}
	
/* Widget Options */
	
	function form($instance) {
		$defaults = array('button_text' => "Sign Up!", 'name_input' => "E.g. Your Full Name", 'email_input' => 'E.g. Your Email Address');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php printf('%s', esc_attr((string)$instance['title'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'thesislove'); ?></label>
			<textarea id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" class="widefat" rows="3" cols="8"><?php printf('%s', esc_textarea($instance['description'])); ?></textarea>
		</p>
        <div align="center"><h3>Aweber Options</h3></div>
        <p>
			<label for="<?php echo $this->get_field_id('listname'); ?>"><?php _e('List Name [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('listname'); ?>" name="<?php echo $this->get_field_name('listname'); ?>" value="<?php printf('%s', esc_attr((string)$instance['listname'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('web_tracking'); ?>"><?php _e('Web Tracking ID [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('web_tracking'); ?>" name="<?php echo $this->get_field_name('web_tracking'); ?>" value="<?php printf('%s', esc_attr((string)$instance['web_tracking'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('email_input'); ?>"><?php _e('Email Input Text [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('email_input'); ?>" name="<?php echo $this->get_field_name('email_input'); ?>" value="<?php printf('%s', esc_attr((string)$instance['email_input'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('name_input'); ?>"><?php _e('Name Input Text [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('name_input'); ?>" name="<?php echo $this->get_field_name('name_input'); ?>" value="<?php printf('%s', esc_attr((string)$instance['name_input'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('button_text'); ?>"><?php _e('Button Text', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('button_text'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" value="<?php printf('%s', esc_attr((string)$instance['button_text'])); ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('button_color'); ?>"><?php _e('Button Color:', 'thesislove'); ?></label>
			<select id="<?php echo $this->get_field_id('button_color'); ?>" name="<?php echo $this->get_field_name('button_color'); ?>">
            <option <?php if('black' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('black', 'thesislove') ?></option>
            <option <?php if('purple' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('purple', 'thesislove') ?></option>
            <option <?php if('rosy' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('rosy', 'thesislove') ?></option>
            <option <?php if('orange' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('orange', 'thesislove') ?></option>
            <option <?php if('yellow' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('yellow', 'thesislove') ?></option>
            <option <?php if('pink' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('pink', 'thesislove') ?></option>
			<option <?php if('red' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('red', 'thesislove') ?></option>	
			<option <?php if('blue' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('blue', 'thesislove') ?></option>	
		    <option <?php if('green' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('green', 'thesislove') ?></option>
			<option <?php if('brown' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('brown', 'thesislove') ?></option>
            <option <?php if('magenta' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('magenta', 'thesislove') ?></option>
			</select>
		</p>
	<?php
	}
}


/* ============[[  ThesisLove -> Mailchimp Optin Form Widget ]]=============== */



/* Registering The Widget */

$widgets = array('mailchimp_thesislove');
foreach ($widgets as $widget)
	register_widget($widget);

class mailchimp_thesislove extends WP_Widget {
	function mailchimp_thesislove() {
		$widget_ops = array('classname' => 'thesislove-optin-form', 'description' => __('Easy Create An Optin Form For Your Mailchimp List. Wait - you can create as many as you want.', 'thesislove'));
		$control_ops = array('id_base' => 'mailchimp_thesislove');
		$this->WP_Widget('mailchimp_thesislove', __('ThesisLove &#8594; Mailchimp Optin Form ', 'thesislove'), $widget_ops, $control_ops);
}
	
	function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$description = $instance['description'];
		$email_input = $instance['email_input'];
		$name_input = $instance['name_input'];
		$button_color = $instance['button_color'];
		$button_text = $instance['button_text'];
		$listname = $instance['listname'];
		$web_tracking = $instance['web_tracking'];
		
/* Widget In The Frontend */
		
			?>
            <? echo $before_widget; ?>
            <? if (!empty($title))
      echo $before_title . $title . $after_title;; ?>
            <div class="form_desc"><p><? echo $description ?></p></div>
            
        <div class="thesislove-optin">
<form action="<? echo $listname ?>" method="post" target="_blank">
<p><input type="text" name="FNAME" class="name" placeholder="<? echo $name_input ?>" tabindex="600" /></p>
<p><input class="email" type="email" name="email" placeholder="<? echo $email_input ?>" tabindex="601" /></p>
<p><input class="button <? echo $button_color ?>" name="submit" type="submit" value="<? echo $button_text ?>" tabindex="602" /></p>
</form>
</div>
<? echo $after_widget; ?>
<?
}
	
/* Widget Update Functions */
			
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = sprintf('%s', strip_tags(stripslashes($new_instance['title'])));
		$instance['description'] = $new_instance['description'];
		$instance['email_input'] = sprintf('%s', wp_kses_data($new_instance['email_input']));
		$instance['name_input'] = sprintf('%s', strip_tags(stripslashes($new_instance['name_input'])));
		$instance['button_color'] = $new_instance['button_color'];
		$instance['button_text'] = sprintf('%s', strip_tags(stripslashes($new_instance['button_text'])));
		$instance['listname'] = $new_instance['listname'];
		$instance['web_tracking'] = $new_instance['web_tracking'];
		return $instance;
	}
	
/* Widget Options */
	
	function form($instance) {
		$defaults = array('button_text' => "Sign Up!", 'name_input' => "E.g. Your Full Name", 'email_input' => 'E.g. Your Email Address');
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php printf('%s', esc_attr((string)$instance['title'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'thesislove'); ?></label>
			<textarea id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" class="widefat" rows="3" cols="8"><?php printf('%s', esc_textarea($instance['description'])); ?></textarea>
		</p>
        <div align="center"><h3>Mailchimp Options</h3></div>
        <p>
			<label for="<?php echo $this->get_field_id('listname'); ?>"><?php _e('Form URL [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('listname'); ?>" name="<?php echo $this->get_field_name('listname'); ?>" value="<?php printf('%s', esc_attr((string)$instance['listname'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('email_input'); ?>"><?php _e('Email Input Text [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('email_input'); ?>" name="<?php echo $this->get_field_name('email_input'); ?>" value="<?php printf('%s', esc_attr((string)$instance['email_input'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('name_input'); ?>"><?php _e('Name Input Text [<a target="_blank" href="http://thesislove.com/forum">?</a>]', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('name_input'); ?>" name="<?php echo $this->get_field_name('name_input'); ?>" value="<?php printf('%s', esc_attr((string)$instance['name_input'])); ?>" class="widefat" type="text" />
		</p>
        <p>
			<label for="<?php echo $this->get_field_id('button_text'); ?>"><?php _e('Button Text', 'thesislove'); ?></label>
			<input id="<?php echo $this->get_field_id('button_text'); ?>" name="<?php echo $this->get_field_name('button_text'); ?>" value="<?php printf('%s', esc_attr((string)$instance['button_text'])); ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('button_color'); ?>"><?php _e('Button Color:', 'thesislove'); ?></label>
			<select id="<?php echo $this->get_field_id('button_color'); ?>" name="<?php echo $this->get_field_name('button_color'); ?>">
            <option <?php if('black' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('black', 'thesislove') ?></option>
            <option <?php if('purple' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('purple', 'thesislove') ?></option>
            <option <?php if('rosy' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('rosy', 'thesislove') ?></option>
            <option <?php if('orange' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('orange', 'thesislove') ?></option>
            <option <?php if('yellow' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('yellow', 'thesislove') ?></option>
            <option <?php if('pink' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('pink', 'thesislove') ?></option>
			<option <?php if('red' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('red', 'thesislove') ?></option>	
			<option <?php if('blue' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('blue', 'thesislove') ?></option>	
		    <option <?php if('green' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('green', 'thesislove') ?></option>
			<option <?php if('brown' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('brown', 'thesislove') ?></option>
            <option <?php if('magenta' == $instance['button_color']) echo "selected=\"selected\""; ?>><?php _e('magenta', 'thesislove') ?></option>
			</select>
		</p>
	<?php
	}
}

/* ============[[  ThesisLove -> Video Embed Widget ]]=============== */

class video_thesislove extends WP_Widget {

	//main construct
	function __construct() {
		
		// define widget class and description
		$widget_ops = array(
			'classname' => 'video_widget_thesislove', 
			'description' => 'Your Videos Into Your Sidebar/Footer - Perfect Embedding.'
		);
		
		
		// register the widget
        $this->WP_Widget('video_thesislove', __('ThesisLove &#8594; Video Embed', 'thesislove'), $widget_ops);
	}
	

	// update the widget when new options have been entered
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['video_url'] = strip_tags($new_instance['video_url']);
		$instance['video_description'] = strip_tags($new_instance['video_description']);
		return $instance;
	}
	

	// print the widget option form on the widget management screen
	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => 'Video', 'id' => '', 'video_url' => '', 'video_description' => '' ) );
		$title = strip_tags($instance['title']);
		$video_url = strip_tags($instance['video_url']);
		$video_description = strip_tags($instance['video_description']);
	?>
		<p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
            <?php _e('Title:', 'thesislove'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        
		<p>
            <label for="<?php echo $this->get_field_id('video_url'); ?>">
            <?php _e('Video URL ', 'thesislove'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('video_url'); ?>" name="<?php echo $this->get_field_name('video_url'); ?>" type="text" value="<?php echo esc_attr($video_url); ?>" />
            <span style="display:block;padding:5px 0" class="description"><?php _e('Enter in a video URL that is compatible with WordPress\'s built-in oEmbed feature.', 'thesislove'); ?> <a href="http://codex.wordpress.org/Embeds" target="_blank"><?php _e('Read More', 'thesislove'); ?></a></span>
        </p>
        
		<p>
            <label for="<?php echo $this->get_field_id('video_description'); ?>">
            <?php _e('Description', 'thesislove'); ?></label>
            <textarea rows="5" class="widefat" id="<?php echo $this->get_field_id('video_description'); ?>" name="<?php echo $this->get_field_name('video_description'); ?>" type="text"><?php echo stripslashes($instance['video_description']); ?></textarea>
        </p>
        
	<?php }
	
	
	// display the widget in the theme
	function widget($args, $instance) {
		extract( $args );
		
		//before widget hook
		echo $before_widget;
		
		//show widget title
		$title = apply_filters( 'widget_title', $instance['title'] );
		if ( $title )
			echo $before_title . $title . $after_title;
		
		// define video height and width
		$video_size = array(
			'width' => 300
		);
		
		// show video
		if( $instance['video_url'] )  { echo '<div class="video_thesislove">' . wp_oembed_get( $instance['video_url'], $video_size ) . '</div>';
		} else {  _e('Please enter a video URL - it seems to be empty!', 'thesislove' ); }
		
		// show video description if field isn't empty
		if( $instance['video_description'] )
			echo '<div class="video_widget_thesislove-desc">'. $instance['video_description']. '</div>';
		echo $after_widget;		
	}
	
}
// register widget
add_action('widgets_init', create_function('', 'return register_widget("video_thesislove");'));	


/* ============[[  ThesisLove -> Featured Posts Widget ]]=============== */

class thesislove_featured_posts extends WP_Widget {
    /** constructor */
    function thesislove_featured_posts() {
        $widget_ops = array('classname' => 'thesislove-featured-posts-widget', 'description' => __('Featured Posts with Thumbnails - either from a specific category or just your recent posts.', 'thesislove'));
		$control_ops = array('id_base' => 'thesislove_featured_posts_widget');
		$this->WP_Widget('thesislove_featured_posts_widget', __('ThesisLove &#8594; Featured Posts ', 'thesislove'), $widget_ops, $control_ops);
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
		$category = apply_filters('widget_title', $instance['category']);
        $number = apply_filters('widget_title', $instance['number']);
        $offset = apply_filters('widget_title', $instance['offset']); ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
							<ul class="thesislove-featured-posts">
							<?php
								global $post;
								$args = array(
									'numberposts' => $number,
									'offset'=> $offset,
									'category' => $category
								);
								$myposts = get_posts( $args );
								foreach( $myposts as $post ) : setup_postdata($post);
								if ( has_post_thumbnail() ) {  ?>
									<li class="clearfix">
                                    	<div class="thesislove-featured-posts-thumb">
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ), 80, 70, true ); ?>" alt="<?php the_title(); ?>" /></a> 
                                        </div>
                                        <div class="thesislove-featured-posts-description">
                                        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="title"><?php the_title(); ?></a>
                                        	<div class="date"><?php echo get_the_date(); ?></div>
                                        </div>
                                    </li>
                               <?php
                               } endforeach; wp_reset_postdata(); ?>
							</ul>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
	$instance['category'] = strip_tags($new_instance['category']);
	$instance['number'] = strip_tags($new_instance['number']);
	$instance['offset'] = strip_tags($new_instance['offset']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {	
	    $instance = wp_parse_args( (array) $instance, array(
			'title' => __('Featured Posts','thesislove'),
			'category' => '',
			'number' => '5',
			'offset'=> '0'
		));					
        $title = esc_attr($instance['title']);
		$category = esc_attr($instance['category']);
        $number = esc_attr($instance['number']);
        $offset = esc_attr($instance['offset']); ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'thesislove'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title','thesislove'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p align="center">Please Note That <strong>Only Posts Having The Wordpress Featured</strong> Image Will Show Up Here</p>
        <p>
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Select a Category:', 'thesislove'); ?></label>
            <br />
            <select name="<?php echo $this->get_field_name('category'); ?>" id="<?php echo $this->get_field_id('category'); ?>">
            <option value="all-cats" <?php if($category == 'all-cats') { ?>selected="selected"<?php } ?>><?php _e('All', 'thesislove'); ?></option>
            <?php
            //get terms
            $cat_terms = get_terms('category', array( 'hide_empty' => '1' ) );
            foreach ( $cat_terms as $cat_term) { ?>
                <option value="<?php echo $cat_term->term_id; ?>" id="<?php echo $cat_term->term_id; ?>" <?php if($category == $cat_term->term_id) { ?>selected="selected"<?php } ?>><?php echo $cat_term->name; ?></option>
            <?php } ?>
            </select>
        </p>
        
		<p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number Of Posts to Show:', 'thesislove'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
		<p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Offset (the number of posts to skip):', 'thesislove'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('offset'); ?>" name="<?php echo $this->get_field_name('offset'); ?>" type="text" value="<?php echo $offset; ?>" />
        </p>
        <?php
    }


}
// register widget
add_action('widgets_init', create_function('', 'return register_widget("thesislove_featured_posts");'));	