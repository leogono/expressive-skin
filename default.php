<?php

function expressive_defaults() {
	return array (
  'css' => '/* 
-- Expressive - A Skin By ThesisLove.
-- Developed and Designed by - Raaj Trambadia.
-- For any support, visit thesislove.com/forum.

 					------- Read The Following -------

-- DO NOT edit anything here - neither the codes nor the variables. Any possible changes can be made from the \\\'Skin Design\\\' page.

-- For any additional CSS codes, use your \\\'Custom CSS\\\' area - that\\\'s because whenever the skin is updated, all your additions on this page will be removed automatically. 

-- However, any additions/tweaks in the \\\'Custom CSS\\\' area will remain as they are.

 					------- Read The Above -------

...If I were you, I wouldn\\\'t go any further and would rather go to my Custom CSS Page. Period.

*/

/* Main Body */

body { font-family: $font; font-size: $size; background-image: $bgimg; color: $dark; }
a { color: $primary;}
a:hover { color: $hover; }
#searchsubmit { background-color: $primary; border-color: $primary; }
#searchsubmit:hover { background-color: $hover; border-color: $hover; }

/* Header Area */
#header .logo a:hover, #header #site_title a:hover { color: $primary; }
#header .logo a, #header #site_title a { font-family: $titles; color: $dark; }

/* Header Menu Configurations Not Required */

/* Site Layout */
.content { width: $content; }
.sidebar { width: $sidebar; }
.page { width: $width; margin: 0 auto; } 

/* Secondary Menu */

.menu a, .menu .current ul a, .menu .current-cat ul a, .menu .current-menu-item ul a, .nav-container { background-color: $dark; }
.menu a:hover, .menu .current ul a:hover, .menu .current-cat ul a:hover, .menu .current-parent a:hover, .menu .current-menu-item ul a:hover, .menu .current-menu-ancestor a:hover, .menu .current a, .menu .current a:hover, .menu .current-cat a, .menu .current-cat a:hover, .menu .current-menu-item a, .menu .current-menu-item a:hover, .menu .current-parent > a, .menu .current-cat-parent > a, .menu .current-menu-ancestor > a { background-color: $primary; }

/* Single Post Styles */

.content .postbox h1, .content .postbox .headline, .post-headline h1 { font-size: $titlessize; color: $dark; font-family: $titles; }
.post-headline a { font-family: $titles; color: $dark; }
.post-headline a:hover { color: $primary; }
.post_image:hover, .thumb:hover, .wp-post-image:hover { background: $primary; }
.page-numbers.current { background: none repeat scroll 0 0 $primary;}
.page-numbers:hover { background: none repeat scroll 0 0 $primary; }
.page-numbers.dots:hover { background: none repeat scroll 0 0 $primary; }
a.comment-reply-link { background-color: $dark; }
a:hover.comment-reply-link { background-color: $primary; } 
#comment_form_submit .input_submit { background: $dark; }
#comment_form_submit .input_submit:hover { background: $primary; }

/* Widgets and Misc. */

h4.widget_title { font-family: $titles; }
input:focus, textarea:focus { border-color: $primary; -moz-box-shadow: 0 0 5px $primary; -webkit-box-shadow: 0 0 5px $primary; box-shadow: 0 0 5px $primary; }
.footer { background-image: $ftimg; }',
  'boxes' => 
  array (
    'thesis_html_container' => 
    array (
      'thesis_html_container_1358680291' => 
      array (
        'html' => 'header',
        'id' => 'header',
        'class' => 'clearfix',
        '_name' => 'Header Area',
      ),
      'thesis_html_container_1358680365' => 
      array (
        'class' => 'page',
        '_name' => 'Header Area (Page)',
      ),
      'thesis_html_container_1359035123' => 
      array (
        'class' => 'page',
        '_name' => 'Secondary Nav Area (Page)',
      ),
      'thesis_html_container_1359055283' => 
      array (
        'html' => 'nav',
        'class' => 'nav-container clearfix',
        '_name' => 'Secondary Nav Area',
      ),
      'thesis_html_container_1359396199' => 
      array (
        'class' => 'main-content clearfix',
        '_name' => 'Content + Sidebar',
      ),
      'thesis_html_container_1359396262' => 
      array (
        'class' => 'page',
        '_name' => 'Main Content (Page)',
      ),
      'thesis_html_container_1359396374' => 
      array (
        'class' => 'content',
        '_name' => 'Content',
      ),
      'thesis_html_container_1359396395' => 
      array (
        'class' => 'sidebar',
        '_name' => 'Sidebar',
      ),
      'thesis_html_container_1359396661' => 
      array (
        'class' => 'post-headline',
        '_name' => 'Headline Area',
      ),
      'thesis_html_container_1359396692' => 
      array (
        'class' => 'post-byline',
        '_name' => 'Post ByLine',
      ),
      'thesis_html_container_1359397152' => 
      array (
        'html' => 'footer',
        'class' => 'footer_widgets clearfix',
        '_name' => 'Footer Widgets',
      ),
      'thesis_html_container_1359483770' => 
      array (
        'class' => 'byline-item',
        '_name' => 'Byline Item',
      ),
      'thesis_html_container_1359483966' => 
      array (
        'class' => 'byline-item',
        '_name' => 'Byline Item',
      ),
      'thesis_html_container_1359483983' => 
      array (
        'class' => 'byline-item',
        '_name' => 'Byline Item',
      ),
      'thesis_html_container_1359484001' => 
      array (
        'class' => 'byline-item',
        '_name' => 'Byline Item',
      ),
      'thesis_html_container_1359484534' => 
      array (
        'class' => 'post-intro',
        '_name' => 'Post Excerpt & Image',
      ),
      'thesis_html_container_1359541185' => 
      array (
        'class' => 'sidebar_widgets',
        '_name' => 'Sidebar Widgets',
      ),
      'thesis_html_container_1359617444' => 
      array (
        'html' => 'nav',
        'class' => 'header-menu clearfix',
        '_name' => 'Header Nav Area',
      ),
      'thesis_html_container_1359617528' => 
      array (
        'class' => 'page clearfix',
        '_name' => 'Header Nav Area (Page)',
      ),
      'thesis_html_container_1359914213' => 
      array (
        'html' => 'footer',
        'class' => 'footer clearfix',
        '_name' => 'Footer',
      ),
      'thesis_html_container_1359914236' => 
      array (
        'class' => 'page',
        '_name' => 'Footer Area (Page)',
      ),
      'thesis_html_container_1359914317' => 
      array (
        'class' => 'footer-widget',
        '_name' => 'Footer Widgets (1)',
      ),
      'thesis_html_container_1359914322' => 
      array (
        'class' => 'footer-widget',
        '_name' => 'Footer Widgets (2)',
      ),
      'thesis_html_container_1359914330' => 
      array (
        'class' => 'footer-widget',
        '_name' => 'Footer Widgets (3)',
      ),
      'thesis_html_container_1359916490' => 
      array (
        'class' => 'footer-att clearfix',
        '_name' => 'Footer Attribution Area',
      ),
      'thesis_html_container_1361707187' => 
      array (
        'class' => 'header-widget',
        '_name' => 'Header Widget',
      ),
      'thesis_html_container_1361727492' => 
      array (
        'class' => 'pre-footer-area page clearfix',
        '_name' => 'Pre-Footer Area',
      ),
      'thesis_html_container_1361727509' => 
      array (
        'class' => 'pre-footer-column',
        '_name' => 'Pre-Footer Area 1',
      ),
      'thesis_html_container_1361727515' => 
      array (
        'class' => 'pre-footer-column',
        '_name' => 'Pre-Footer Area 2',
      ),
      'thesis_html_container_1361727520' => 
      array (
        'class' => 'pre-footer-column last-pre-footer',
        '_name' => 'Pre-Footer Area 3',
      ),
    ),
    'thesis_wp_nav_menu' => 
    array (
      'thesis_wp_nav_menu_1358680432' => 
      array (
        'menu' => '2',
        '_name' => 'Secondary Menu',
      ),
      'thesis_wp_nav_menu_1359617618' => 
      array (
        'menu' => '2',
        'menu_class' => 'header-nav',
        '_name' => 'Header Menu',
      ),
    ),
    'thesis_post_box' => 
    array (
      'thesis_post_box_1359404770' => 
      array (
        'html' => 'article',
        'class' => 'postbox',
        '_admin' => 
        array (
          'open' => true,
        ),
        '_name' => 'Post Box (Single)',
      ),
      'thesis_post_box_1359404946' => 
      array (
        'class' => 'postbox',
        '_name' => 'Post Box (Homepage)',
      ),
    ),
    'thesis_wp_widgets' => 
    array (
      'thesis_wp_widgets_1359396889' => 
      array (
        'class' => 'clearfix',
        '_name' => 'Sidebar Widgets',
      ),
      'thesis_wp_widgets_1359914291' => 
      array (
        '_name' => 'Footer Widgets 1',
      ),
      'thesis_wp_widgets_1359914295' => 
      array (
        '_name' => 'Footer Widgets 2',
      ),
      'thesis_wp_widgets_1359914300' => 
      array (
        '_name' => 'Footer Widgets 3',
      ),
      'thesis_wp_widgets_1361706991' => 
      array (
        '_name' => 'Header Ad Widget',
      ),
      'thesis_wp_widgets_1361727544' => 
      array (
        '_name' => 'Pre-Footer Widgets 1',
      ),
      'thesis_wp_widgets_1361727549' => 
      array (
        '_name' => 'Pre-Footer Widgets 2',
      ),
      'thesis_wp_widgets_1361727553' => 
      array (
        '_name' => 'Pre-Footer Widgets 3',
      ),
    ),
    'thesis_comments' => 
    array (
      'thesis_comments_1359403862' => 
      array (
        'html' => 'div',
        'class' => 'comments',
        '_name' => 'Comments',
      ),
    ),
    'thesis_post_headline' => 
    array (
      'thesis_post_box_1359404946_thesis_post_headline' => 
      array (
        'link' => 
        array (
          'on' => true,
        ),
        '_parent' => 'thesis_post_box_1359404946',
      ),
    ),
    'thesis_post_content' => 
    array (
      'thesis_post_box_1359404770_thesis_post_content' => 
      array (
        'class' => 'post_text',
        '_parent' => 'thesis_post_box_1359404770',
      ),
    ),
    'thesis_post_author' => 
    array (
      'thesis_post_box_1359404946_thesis_post_author' => 
      array (
        'link' => 
        array (
          'on' => true,
        ),
        'nofollow' => 
        array (
          'nofollow' => true,
        ),
        '_parent' => 'thesis_post_box_1359404946',
      ),
      'thesis_post_box_1359404770_thesis_post_author' => 
      array (
        'link' => 
        array (
          'on' => true,
        ),
        'nofollow' => 
        array (
          'nofollow' => true,
        ),
        '_parent' => 'thesis_post_box_1359404770',
      ),
    ),
    'thesis_post_num_comments' => 
    array (
      'thesis_post_box_1359404946_thesis_post_num_comments' => 
      array (
        'display' => 
        array (
          'closed' => false,
        ),
        '_parent' => 'thesis_post_box_1359404946',
      ),
    ),
    'thesis_post_categories' => 
    array (
      'thesis_post_box_1359404946_thesis_post_categories' => 
      array (
        'html' => 'div',
        'separator' => ',',
        'nofollow' => 
        array (
          'on' => true,
        ),
        '_parent' => 'thesis_post_box_1359404946',
      ),
    ),
    'thesis_comment_form' => 
    array (
      'thesis_comment_form_1359574679' => 
      array (
        '_name' => 'Comment Form',
      ),
    ),
    'thesis_comment_form_name' => 
    array (
      'thesis_comment_form_1359574679_thesis_comment_form_name' => 
      array (
        'placeholder' => 'Your Name',
        '_parent' => 'thesis_comment_form_1359574679',
      ),
    ),
    'thesis_comment_form_email' => 
    array (
      'thesis_comment_form_1359574679_thesis_comment_form_email' => 
      array (
        'placeholder' => 'Your Email Address',
        '_parent' => 'thesis_comment_form_1359574679',
      ),
    ),
    'thesis_comment_form_url' => 
    array (
      'thesis_comment_form_1359574679_thesis_comment_form_url' => 
      array (
        'placeholder' => 'Your Website URL',
        '_parent' => 'thesis_comment_form_1359574679',
      ),
    ),
    'ca_menu' => 
    array (
      'ca_menu' => 
      array (
        'icon' => '@',
        'headline' => 'Test Headline',
        'description' => 'Some Description',
      ),
    ),
    'thesis_text_box' => 
    array (
      'thesis_text_box_1362303699' => 
      array (
        'text' => '<a href="#" id="pull">Menu</a>',
        'filter' => 
        array (
          'on' => true,
        ),
        'html' => 'none',
        '_name' => 'Responsive Menu',
      ),
    ),
    'fb_item_2' => 
    array (
      'feature_boxes_1360175172_fb_item_2' => 
      array (
        'icon' => 'icon-cloud-download',
        'headline' => 'Easy Downloads',
        'description' => 'Whether you become a member or buy the skin individually - you get your product the very next second you finish the required.',
        'buttonclass' => 'button blue',
        'buttontext' => 'Purchase This Skin',
        'link' => 'http://thesislove.com/gallery/',
        '_parent' => 'feature_boxes_1360175172',
      ),
    ),
    'fb_item_3' => 
    array (
      'feature_boxes_1360175172_fb_item_3' => 
      array (
        'icon' => 'icon-money',
        'headline' => 'Affiliate Program',
        'description' => 'Customer or not, you can always make money with us. Join our affiliate program and start earning money - monthly payments directly to PayPal!',
        'buttonclass' => 'button orange',
        'buttontext' => 'Become An Affiliate',
        'link' => 'http://thesislove.com/affiliates/',
        '_parent' => 'feature_boxes_1360175172',
      ),
    ),
    'thesis_post_tags' => 
    array (
      'thesis_post_box_1359404770_thesis_post_tags' => 
      array (
        'intro' => 'Post Tags - ',
        'separator' => ',',
        '_parent' => 'thesis_post_box_1359404770',
      ),
    ),
    'thesis_previous_post_link' => 
    array (
      'thesis_previous_post_link' => 
      array (
        'html' => 'p',
        'intro' => 'The Previous Post - ',
      ),
    ),
    'thesis_next_post_link' => 
    array (
      'thesis_next_post_link' => 
      array (
        'html' => 'p',
        'intro' => 'The Next Post - ',
      ),
    ),
    'full_optin_bar' => 
    array (
      'full_optin_bar' => 
      array (
        'headline' => 'Some Headline',
        'submit' => 'Submit Text',
      ),
    ),
    'raaj_box' => 
    array (
      'raaj_box_1361061709' => 
      array (
        '_name' => 'Raajs Box (ThesisLove)',
      ),
      'raaj_box_1362392082' => 
      array (
        '_name' => 'Raajs Box (ThesisLove)',
      ),
      'raaj_box_1362392114' => 
      array (
        'class' => 'ca-menu clearfix',
        '_name' => 'Raajs Box (ThesisLove)',
      ),
    ),
    'ca_menu_1' => 
    array (
      'raaj_box_1361061709_ca_menu_1' => 
      array (
        'icon' => 'icon-twitter',
        'link' => '#',
        'headline' => 'Some Headline',
        'description' => 'Some DescriptionSome DescriptionSome DescriptionSome DescriptionSome Description',
        '_parent' => 'raaj_box_1361061709',
      ),
      'raaj_box_1362392114_ca_menu_1' => 
      array (
        'icon' => 'icon-th-large',
        'link' => '#',
        'headline' => 'Some Headline',
        'description' => 'This Is Some Description...This Is Some Description...This Is Some Description..This Is Some Description',
        '_parent' => 'raaj_box_1362392114',
      ),
    ),
    'thesis_comment_form_submit' => 
    array (
      'thesis_comment_form_1359574679_thesis_comment_form_submit' => 
      array (
        'text' => 'Submit Comment',
        '_parent' => 'thesis_comment_form_1359574679',
      ),
    ),
    'diywp_related_posts_box' => 
    array (
      'diywp_related_posts_box' => 
      array (
        'title' => 'SOmethingasdas das',
      ),
    ),
    'main_promo_box' => 
    array (
      'promo_box_main_promo_box' => 
      array (
        'buttoncolor' => 'red',
        '_parent' => 'promo_box',
      ),
    ),
    'ca_menu_2' => 
    array (
      'raaj_box_1362392114_ca_menu_2' => 
      array (
        'icon' => 'icon-gift',
        'link' => '#',
        'headline' => 'Another Headline',
        'description' => 'This Is Some Description...This Is Some Description...This Is Some Description..This Is Some Description',
        '_parent' => 'raaj_box_1362392114',
      ),
    ),
    'ca_menu_3' => 
    array (
      'raaj_box_1362392114_ca_menu_3' => 
      array (
        'icon' => 'icon-twitter',
        'link' => '#',
        'headline' => 'Some Headline',
        'description' => 'This Is Some Description...This Is Some Description...This Is Some Description..This Is Some Description',
        '_parent' => 'raaj_box_1362392114',
      ),
    ),
    'ca_menu_4' => 
    array (
      'raaj_box_1362392114_ca_menu_4' => 
      array (
        'icon' => 'icon-facebook',
        'link' => '#',
        'headline' => 'Some Headline',
        'description' => 'This Is Some Description...This Is Some Description...This Is Some Description..This Is Some Description',
        '_parent' => 'raaj_box_1362392114',
      ),
    ),
    'single_optin' => 
    array (
      'single_optin' => 
      array (
        'buttoncolor' => 'orange',
      ),
    ),
    'single_optin_mailchimp' => 
    array (
      'single_optin_mailchimp' => 
      array (
        'buttoncolor' => 'yellow',
      ),
    ),
  ),
  'vars' => 
  array (
    'var_1358876913' => 
    array (
      'name' => 'Primary Text Color',
      'ref' => 'dark',
      'css' => '#0a0a0a',
    ),
    'var_1358963961' => 
    array (
      'name' => 'Grey',
      'ref' => 'grey',
      'css' => '#5e5e5e',
    ),
    'var_1358964144' => 
    array (
      'name' => 'Primary Link Color',
      'ref' => 'primary',
      'css' => '#009900',
    ),
    'var_1358969019' => 
    array (
      'name' => 'Primary Hover Color',
      'ref' => 'hover',
      'css' => '#005500',
    ),
    'var_1359402697' => 
    array (
      'name' => 'Primary Titles Font',
      'ref' => 'titles',
      'css' => 'Signika; font-weight: 600',
    ),
    'var_1362441211' => 
    array (
      'name' => 'Main Content Width (Content + Sidebar)',
      'ref' => 'width',
      'css' => '1040px',
    ),
    'var_1362474407' => 
    array (
      'name' => 'Primary Content Width',
      'ref' => 'content',
      'css' => '700px',
    ),
    'var_1362474524' => 
    array (
      'name' => 'Primary Sidebar Width',
      'ref' => 'sidebar',
      'css' => '340px',
    ),
    'var_1365591240' => 
    array (
      'name' => 'Font',
      'ref' => 'font',
      'css' => '"Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS", sans-serif',
    ),
    'var_1365591602' => 
    array (
      'name' => 'Font Size',
      'ref' => 'size',
      'css' => '16px',
    ),
    'var_1365594287' => 
    array (
      'name' => 'Titles Font Size',
      'ref' => 'titlessize',
      'css' => '26px',
    ),
    'var_1365621511' => 
    array (
      'name' => 'Background Image',
      'ref' => 'bgimg',
      'css' => 'url(images/b-pattern.gif)',
    ),
    'var_1365686015' => 
    array (
      'name' => 'Footer Background Image',
      'ref' => 'ftimg',
      'css' => 'url(images/dark_brick_wall.png)',
    ),
  ),
  'templates' => 
  array (
    'home' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1359617444',
          1 => 'thesis_html_container_1358680291',
          2 => 'thesis_html_container_1359055283',
          3 => 'thesis_html_container_1359396199',
          4 => 'thesis_html_container_1361727492',
          5 => 'thesis_html_container_1359914213',
        ),
        'thesis_html_container_1359617444' => 
        array (
          0 => 'thesis_html_container_1359617528',
        ),
        'thesis_html_container_1359617528' => 
        array (
          0 => 'thesis_wp_nav_menu_1359617618',
        ),
        'thesis_html_container_1358680291' => 
        array (
          0 => 'thesis_html_container_1358680365',
        ),
        'thesis_html_container_1358680365' => 
        array (
          0 => 'thesis_site_title',
          1 => 'thesis_html_container_1361707187',
          2 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1361707187' => 
        array (
          0 => 'thesis_wp_widgets_1361706991',
        ),
        'thesis_html_container_1359055283' => 
        array (
          0 => 'thesis_html_container_1359035123',
        ),
        'thesis_html_container_1359035123' => 
        array (
          0 => 'thesis_wp_nav_menu_1358680432',
          1 => 'thesis_text_box_1362303699',
        ),
        'thesis_html_container_1359396199' => 
        array (
          0 => 'thesis_html_container_1359396262',
        ),
        'thesis_html_container_1359396262' => 
        array (
          0 => 'thesis_html_container_1359396374',
          1 => 'thesis_html_container_1359396395',
        ),
        'thesis_html_container_1359396374' => 
        array (
          0 => 'thesis_wp_loop',
          1 => 'page_nav',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1359404946',
        ),
        'thesis_post_box_1359404946' => 
        array (
          0 => 'thesis_html_container_1359396661',
          1 => 'thesis_html_container_1359484534',
        ),
        'thesis_html_container_1359396661' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_headline',
          1 => 'thesis_html_container_1359396692',
        ),
        'thesis_html_container_1359396692' => 
        array (
          0 => 'thesis_html_container_1359483770',
          1 => 'thesis_html_container_1359483966',
          2 => 'thesis_html_container_1359483983',
          3 => 'thesis_html_container_1359484001',
        ),
        'thesis_html_container_1359483770' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_author',
        ),
        'thesis_html_container_1359483966' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_categories',
        ),
        'thesis_html_container_1359483983' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_date',
        ),
        'thesis_html_container_1359484001' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_num_comments',
        ),
        'thesis_html_container_1359484534' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_image',
          1 => 'thesis_post_box_1359404946_thesis_wp_featured_image',
          2 => 'thesis_post_box_1359404946_thesis_post_excerpt',
        ),
        'thesis_html_container_1359396395' => 
        array (
          0 => 'thesis_html_container_1359541185',
        ),
        'thesis_html_container_1359541185' => 
        array (
          0 => 'thesis_wp_widgets_1359396889',
        ),
        'thesis_html_container_1361727492' => 
        array (
          0 => 'thesis_html_container_1361727509',
          1 => 'thesis_html_container_1361727515',
          2 => 'thesis_html_container_1361727520',
        ),
        'thesis_html_container_1361727509' => 
        array (
          0 => 'thesis_wp_widgets_1361727544',
        ),
        'thesis_html_container_1361727515' => 
        array (
          0 => 'thesis_wp_widgets_1361727549',
        ),
        'thesis_html_container_1361727520' => 
        array (
          0 => 'thesis_wp_widgets_1361727553',
        ),
        'thesis_html_container_1359914213' => 
        array (
          0 => 'thesis_html_container_1359914236',
        ),
        'thesis_html_container_1359914236' => 
        array (
          0 => 'thesis_html_container_1359397152',
          1 => 'thesis_html_container_1359916490',
        ),
        'thesis_html_container_1359397152' => 
        array (
          0 => 'thesis_html_container_1359914317',
          1 => 'thesis_html_container_1359914322',
          2 => 'thesis_html_container_1359914330',
        ),
        'thesis_html_container_1359914317' => 
        array (
          0 => 'thesis_wp_widgets_1359914291',
        ),
        'thesis_html_container_1359914322' => 
        array (
          0 => 'thesis_wp_widgets_1359914295',
        ),
        'thesis_html_container_1359914330' => 
        array (
          0 => 'thesis_wp_widgets_1359914300',
        ),
        'thesis_html_container_1359916490' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesislove_att',
        ),
      ),
    ),
    'single' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1359617444',
          1 => 'thesis_html_container_1358680291',
          2 => 'thesis_html_container_1359055283',
          3 => 'thesis_html_container_1359396199',
          4 => 'thesis_html_container_1359914213',
        ),
        'thesis_html_container_1359617444' => 
        array (
          0 => 'thesis_html_container_1359617528',
        ),
        'thesis_html_container_1359617528' => 
        array (
          0 => 'thesis_wp_nav_menu_1359617618',
        ),
        'thesis_html_container_1358680291' => 
        array (
          0 => 'thesis_html_container_1358680365',
        ),
        'thesis_html_container_1358680365' => 
        array (
          0 => 'thesis_site_title',
          1 => 'thesis_html_container_1361707187',
          2 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1361707187' => 
        array (
          0 => 'thesis_wp_widgets_1361706991',
        ),
        'thesis_html_container_1359055283' => 
        array (
          0 => 'thesis_html_container_1359035123',
        ),
        'thesis_html_container_1359035123' => 
        array (
          0 => 'thesis_wp_nav_menu_1358680432',
          1 => 'thesis_text_box_1362303699',
        ),
        'thesis_html_container_1359396199' => 
        array (
          0 => 'thesis_html_container_1359396262',
        ),
        'thesis_html_container_1359396262' => 
        array (
          0 => 'thesis_html_container_1359396374',
          1 => 'thesis_html_container_1359396395',
        ),
        'thesis_html_container_1359396374' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1359404770',
        ),
        'thesis_post_box_1359404770' => 
        array (
          0 => 'thesis_html_container_1359396661',
          1 => 'thesis_post_box_1359404770_thesis_post_image',
          2 => 'thesis_post_box_1359404770_thesis_post_thumbnail',
          3 => 'thesis_post_box_1359404770_thesis_wp_featured_image',
          4 => 'thesis_post_box_1359404770_thesis_post_content',
          5 => 'thesis_post_box_1359404770_thesis_post_tags',
          6 => 'single_optin',
          7 => 'author_box',
          8 => 'thesis_comments_intro',
          9 => 'thesis_comments_1359403862',
          10 => 'thesis_comment_form_1359574679',
          11 => 'thesis_previous_post_link',
          12 => 'thesis_next_post_link',
        ),
        'thesis_html_container_1359396661' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_headline',
          1 => 'thesis_html_container_1359396692',
        ),
        'thesis_html_container_1359396692' => 
        array (
          0 => 'thesis_html_container_1359484001',
          1 => 'thesis_html_container_1359483770',
          2 => 'thesis_html_container_1359483983',
          3 => 'thesis_html_container_1359483966',
        ),
        'thesis_html_container_1359483770' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_author',
        ),
        'thesis_html_container_1359483983' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_categories',
        ),
        'thesis_html_container_1359483966' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_num_comments',
          1 => 'thesis_post_box_1359404770_thesis_post_date',
        ),
        'thesis_comments_1359403862' => 
        array (
          0 => 'thesis_comments_1359403862_thesis_comment_avatar',
          1 => 'thesis_comments_1359403862_thesis_comment_author',
          2 => 'thesis_comments_1359403862_thesis_comment_date',
          3 => 'thesis_comments_1359403862_thesis_comment_text',
          4 => 'thesis_comments_1359403862_thesis_comment_reply',
        ),
        'thesis_comment_form_1359574679' => 
        array (
          0 => 'thesis_comment_form_1359574679_thesis_comment_form_title',
          1 => 'thesis_comment_form_1359574679_thesis_comment_form_cancel',
          2 => 'thesis_comment_form_1359574679_thesis_comment_form_name',
          3 => 'thesis_comment_form_1359574679_thesis_comment_form_email',
          4 => 'thesis_comment_form_1359574679_thesis_comment_form_url',
          5 => 'thesis_comment_form_1359574679_thesis_comment_form_comment',
          6 => 'thesis_comment_form_1359574679_thesis_comment_form_submit',
        ),
        'thesis_html_container_1359396395' => 
        array (
          0 => 'thesis_html_container_1359541185',
        ),
        'thesis_html_container_1359541185' => 
        array (
          0 => 'thesis_wp_widgets_1359396889',
        ),
        'thesis_html_container_1359914213' => 
        array (
          0 => 'thesis_html_container_1359914236',
        ),
        'thesis_html_container_1359914236' => 
        array (
          0 => 'thesis_html_container_1359397152',
          1 => 'thesis_html_container_1359916490',
        ),
        'thesis_html_container_1359397152' => 
        array (
          0 => 'thesis_html_container_1359914317',
          1 => 'thesis_html_container_1359914322',
          2 => 'thesis_html_container_1359914330',
        ),
        'thesis_html_container_1359914317' => 
        array (
          0 => 'thesis_wp_widgets_1359914291',
        ),
        'thesis_html_container_1359914322' => 
        array (
          0 => 'thesis_wp_widgets_1359914295',
        ),
        'thesis_html_container_1359914330' => 
        array (
          0 => 'thesis_wp_widgets_1359914300',
        ),
        'thesis_html_container_1359916490' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesislove_att',
        ),
      ),
    ),
    'page' => 
    array (
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1359617444',
          1 => 'thesis_html_container_1358680291',
          2 => 'thesis_html_container_1359055283',
          3 => 'thesis_html_container_1359396199',
          4 => 'thesis_html_container_1359914213',
        ),
        'thesis_html_container_1359617444' => 
        array (
          0 => 'thesis_html_container_1359617528',
        ),
        'thesis_html_container_1359617528' => 
        array (
          0 => 'thesis_wp_nav_menu_1359617618',
        ),
        'thesis_html_container_1358680291' => 
        array (
          0 => 'thesis_html_container_1358680365',
        ),
        'thesis_html_container_1358680365' => 
        array (
          0 => 'thesis_site_title',
          1 => 'thesis_html_container_1361707187',
          2 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1361707187' => 
        array (
          0 => 'thesis_wp_widgets_1361706991',
        ),
        'thesis_html_container_1359055283' => 
        array (
          0 => 'thesis_html_container_1359035123',
        ),
        'thesis_html_container_1359035123' => 
        array (
          0 => 'thesis_wp_nav_menu_1358680432',
          1 => 'thesis_text_box_1362303699',
        ),
        'thesis_html_container_1359396199' => 
        array (
          0 => 'thesis_html_container_1359396262',
        ),
        'thesis_html_container_1359396262' => 
        array (
          0 => 'thesis_html_container_1359396374',
          1 => 'thesis_html_container_1359396395',
        ),
        'thesis_html_container_1359396374' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1359404770',
        ),
        'thesis_post_box_1359404770' => 
        array (
          0 => 'thesis_html_container_1359396661',
          1 => 'thesis_post_box_1359404770_thesis_post_image',
          2 => 'thesis_post_box_1359404770_thesis_post_thumbnail',
          3 => 'thesis_post_box_1359404770_thesis_wp_featured_image',
          4 => 'thesis_post_box_1359404770_thesis_post_content',
          5 => 'thesis_post_box_1359404770_thesis_post_tags',
          6 => 'thesis_comments_intro',
          7 => 'thesis_comments_1359403862',
          8 => 'thesis_comment_form_1359574679',
        ),
        'thesis_html_container_1359396661' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_headline',
          1 => 'thesis_html_container_1359396692',
        ),
        'thesis_html_container_1359396692' => 
        array (
          0 => 'thesis_html_container_1359484001',
          1 => 'thesis_html_container_1359483770',
          2 => 'thesis_html_container_1359483983',
          3 => 'thesis_html_container_1359483966',
        ),
        'thesis_html_container_1359483770' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_author',
        ),
        'thesis_html_container_1359483983' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_categories',
        ),
        'thesis_html_container_1359483966' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_num_comments',
          1 => 'thesis_post_box_1359404770_thesis_post_date',
        ),
        'thesis_comments_1359403862' => 
        array (
          0 => 'thesis_comments_1359403862_thesis_comment_avatar',
          1 => 'thesis_comments_1359403862_thesis_comment_author',
          2 => 'thesis_comments_1359403862_thesis_comment_date',
          3 => 'thesis_comments_1359403862_thesis_comment_text',
          4 => 'thesis_comments_1359403862_thesis_comment_reply',
        ),
        'thesis_comment_form_1359574679' => 
        array (
          0 => 'thesis_comment_form_1359574679_thesis_comment_form_title',
          1 => 'thesis_comment_form_1359574679_thesis_comment_form_cancel',
          2 => 'thesis_comment_form_1359574679_thesis_comment_form_name',
          3 => 'thesis_comment_form_1359574679_thesis_comment_form_email',
          4 => 'thesis_comment_form_1359574679_thesis_comment_form_url',
          5 => 'thesis_comment_form_1359574679_thesis_comment_form_comment',
          6 => 'thesis_comment_form_1359574679_thesis_comment_form_submit',
        ),
        'thesis_html_container_1359396395' => 
        array (
          0 => 'thesis_html_container_1359541185',
        ),
        'thesis_html_container_1359541185' => 
        array (
          0 => 'thesis_wp_widgets_1359396889',
        ),
        'thesis_html_container_1359914213' => 
        array (
          0 => 'thesis_html_container_1359914236',
        ),
        'thesis_html_container_1359914236' => 
        array (
          0 => 'thesis_html_container_1359397152',
          1 => 'thesis_html_container_1359916490',
        ),
        'thesis_html_container_1359397152' => 
        array (
          0 => 'thesis_html_container_1359914317',
          1 => 'thesis_html_container_1359914322',
          2 => 'thesis_html_container_1359914330',
        ),
        'thesis_html_container_1359914317' => 
        array (
          0 => 'thesis_wp_widgets_1359914291',
        ),
        'thesis_html_container_1359914322' => 
        array (
          0 => 'thesis_wp_widgets_1359914295',
        ),
        'thesis_html_container_1359914330' => 
        array (
          0 => 'thesis_wp_widgets_1359914300',
        ),
        'thesis_html_container_1359916490' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesislove_att',
        ),
        'thesis_post_box_1359404946' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_headline',
          1 => 'thesis_post_box_1359404946_thesis_post_author',
          2 => 'thesis_post_box_1359404946_thesis_post_edit',
          3 => 'thesis_post_box_1359404946_thesis_post_content',
        ),
        'promo_box' => 
        array (
          0 => 'promo_box_main_promo_box',
        ),
      ),
    ),
    'archive' => 
    array (
      'options' => 
      array (
        'thesis_wp_loop' => 
        array (
          'posts_per_page' => '10',
        ),
      ),
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1359617444',
          1 => 'thesis_html_container_1358680291',
          2 => 'thesis_html_container_1359055283',
          3 => 'thesis_html_container_1359396199',
          4 => 'thesis_html_container_1359914213',
        ),
        'thesis_html_container_1359617444' => 
        array (
          0 => 'thesis_html_container_1359617528',
        ),
        'thesis_html_container_1359617528' => 
        array (
          0 => 'thesis_wp_nav_menu_1359617618',
        ),
        'thesis_html_container_1358680291' => 
        array (
          0 => 'thesis_html_container_1358680365',
        ),
        'thesis_html_container_1358680365' => 
        array (
          0 => 'thesis_site_title',
          1 => 'thesis_html_container_1361707187',
          2 => 'thesis_site_tagline',
        ),
        'thesis_html_container_1361707187' => 
        array (
          0 => 'thesis_wp_widgets_1361706991',
        ),
        'thesis_html_container_1359055283' => 
        array (
          0 => 'thesis_html_container_1359035123',
        ),
        'thesis_html_container_1359035123' => 
        array (
          0 => 'thesis_wp_nav_menu_1358680432',
          1 => 'thesis_text_box_1362303699',
        ),
        'thesis_html_container_1359396199' => 
        array (
          0 => 'thesis_html_container_1359396262',
        ),
        'thesis_html_container_1359396262' => 
        array (
          0 => 'thesis_html_container_1359396374',
          1 => 'thesis_html_container_1359396395',
        ),
        'thesis_html_container_1359396374' => 
        array (
          0 => 'thesis_archive_title',
          1 => 'thesis_archive_content',
          2 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1359404946',
        ),
        'thesis_post_box_1359404946' => 
        array (
          0 => 'thesis_html_container_1359396661',
          1 => 'thesis_html_container_1359484534',
        ),
        'thesis_html_container_1359396661' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_headline',
          1 => 'thesis_html_container_1359396692',
        ),
        'thesis_html_container_1359396692' => 
        array (
          0 => 'thesis_html_container_1359483770',
          1 => 'thesis_html_container_1359483966',
          2 => 'thesis_html_container_1359483983',
          3 => 'thesis_html_container_1359484001',
        ),
        'thesis_html_container_1359483770' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_author',
        ),
        'thesis_html_container_1359483966' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_categories',
        ),
        'thesis_html_container_1359483983' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_date',
        ),
        'thesis_html_container_1359484001' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_num_comments',
        ),
        'thesis_html_container_1359484534' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_image',
          1 => 'thesis_post_box_1359404946_thesis_wp_featured_image',
          2 => 'thesis_post_box_1359404946_thesis_post_excerpt',
        ),
        'thesis_html_container_1359396395' => 
        array (
          0 => 'thesis_html_container_1359541185',
        ),
        'thesis_html_container_1359541185' => 
        array (
          0 => 'thesis_wp_widgets_1359396889',
        ),
        'thesis_html_container_1359914213' => 
        array (
          0 => 'thesis_html_container_1359914236',
        ),
        'thesis_html_container_1359914236' => 
        array (
          0 => 'thesis_html_container_1359397152',
          1 => 'thesis_html_container_1359916490',
        ),
        'thesis_html_container_1359397152' => 
        array (
          0 => 'thesis_html_container_1359914317',
          1 => 'thesis_html_container_1359914322',
          2 => 'thesis_html_container_1359914330',
        ),
        'thesis_html_container_1359914317' => 
        array (
          0 => 'thesis_wp_widgets_1359914291',
        ),
        'thesis_html_container_1359914322' => 
        array (
          0 => 'thesis_wp_widgets_1359914295',
        ),
        'thesis_html_container_1359914330' => 
        array (
          0 => 'thesis_wp_widgets_1359914300',
        ),
        'thesis_html_container_1359916490' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesislove_att',
        ),
        'thesis_post_box_1359404770' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_headline',
          1 => 'thesis_post_box_1359404770_thesis_post_author',
          2 => 'thesis_post_box_1359404770_thesis_post_edit',
          3 => 'thesis_post_box_1359404770_thesis_post_content',
        ),
        'thesis_comments_1359403862' => 
        array (
          0 => 'thesis_comments_1359403862_thesis_comment_author',
          1 => 'thesis_comments_1359403862_thesis_comment_date',
          2 => 'thesis_comments_1359403862_thesis_comment_edit',
          3 => 'thesis_comments_1359403862_thesis_comment_text',
          4 => 'thesis_comments_1359403862_thesis_comment_reply',
        ),
        'thesis_comment_form_1359574679' => 
        array (
          0 => 'thesis_comment_form_1359574679_thesis_comment_form_title',
          1 => 'thesis_comment_form_1359574679_thesis_comment_form_cancel',
          2 => 'thesis_comment_form_1359574679_thesis_comment_form_name',
          3 => 'thesis_comment_form_1359574679_thesis_comment_form_email',
          4 => 'thesis_comment_form_1359574679_thesis_comment_form_url',
          5 => 'thesis_comment_form_1359574679_thesis_comment_form_comment',
          6 => 'thesis_comment_form_1359574679_thesis_comment_form_submit',
        ),
        'promo_box' => 
        array (
          0 => 'promo_box_main_promo_box',
        ),
      ),
    ),
    'custom_1360303061' => 
    array (
      'title' => 'Expressive Landing Page',
      'options' => 
      array (
        'thesis_html_body' => 
        array (
          'class' => 'landing',
        ),
      ),
      'boxes' => 
      array (
        'thesis_html_body' => 
        array (
          0 => 'thesis_html_container_1358680291',
          1 => 'thesis_html_container_1359396199',
          2 => 'thesis_html_container_1359914213',
        ),
        'thesis_html_container_1358680291' => 
        array (
          0 => 'thesis_html_container_1358680365',
        ),
        'thesis_html_container_1358680365' => 
        array (
          0 => 'thesis_site_title',
        ),
        'thesis_html_container_1359396199' => 
        array (
          0 => 'thesis_html_container_1359396262',
        ),
        'thesis_html_container_1359396262' => 
        array (
          0 => 'thesis_html_container_1359396374',
        ),
        'thesis_html_container_1359396374' => 
        array (
          0 => 'thesis_wp_loop',
        ),
        'thesis_wp_loop' => 
        array (
          0 => 'thesis_post_box_1359404770',
        ),
        'thesis_post_box_1359404770' => 
        array (
          0 => 'thesis_html_container_1359396661',
          1 => 'thesis_post_box_1359404770_thesis_post_image',
          2 => 'thesis_post_box_1359404770_thesis_post_thumbnail',
          3 => 'thesis_post_box_1359404770_thesis_wp_featured_image',
          4 => 'thesis_post_box_1359404770_thesis_post_content',
        ),
        'thesis_html_container_1359396661' => 
        array (
          0 => 'thesis_post_box_1359404770_thesis_post_headline',
        ),
        'thesis_html_container_1359914213' => 
        array (
          0 => 'thesis_html_container_1359914236',
        ),
        'thesis_html_container_1359914236' => 
        array (
          0 => 'thesis_html_container_1359916490',
        ),
        'thesis_html_container_1359916490' => 
        array (
          0 => 'thesis_attribution',
          1 => 'thesislove_att',
        ),
        'thesis_html_container_1359035123' => 
        array (
          0 => 'thesis_wp_nav_menu_1358680432',
        ),
        'thesis_html_container_1359396395' => 
        array (
          0 => 'thesis_html_container_1359541185',
        ),
        'thesis_html_container_1359541185' => 
        array (
          0 => 'thesis_wp_widgets_1359396889',
        ),
        'thesis_html_container_1359396692' => 
        array (
          0 => 'thesis_html_container_1359484001',
          1 => 'thesis_html_container_1359483770',
          2 => 'thesis_html_container_1359483983',
          3 => 'thesis_html_container_1359483966',
        ),
        'thesis_html_container_1359397152' => 
        array (
          0 => 'thesis_html_container_1359914317',
          1 => 'thesis_html_container_1359914322',
          2 => 'thesis_html_container_1359914330',
        ),
        'thesis_html_container_1359914317' => 
        array (
          0 => 'thesis_wp_widgets_1359914291',
        ),
        'thesis_html_container_1359914322' => 
        array (
          0 => 'thesis_wp_widgets_1359914295',
        ),
        'thesis_html_container_1359914330' => 
        array (
          0 => 'thesis_wp_widgets_1359914300',
        ),
        'thesis_html_container_1359617444' => 
        array (
          0 => 'thesis_html_container_1359617528',
        ),
        'thesis_html_container_1359617528' => 
        array (
          0 => 'thesis_wp_nav_menu_1359617618',
        ),
        'thesis_post_box_1359404946' => 
        array (
          0 => 'thesis_post_box_1359404946_thesis_post_headline',
          1 => 'thesis_post_box_1359404946_thesis_post_author',
          2 => 'thesis_post_box_1359404946_thesis_post_edit',
          3 => 'thesis_post_box_1359404946_thesis_post_content',
        ),
        'thesis_comments_1359403862' => 
        array (
          0 => 'thesis_comments_1359403862_thesis_comment_avatar',
          1 => 'thesis_comments_1359403862_thesis_comment_author',
          2 => 'thesis_comments_1359403862_thesis_comment_date',
          3 => 'thesis_comments_1359403862_thesis_comment_text',
          4 => 'thesis_comments_1359403862_thesis_comment_reply',
        ),
        'thesis_comment_form_1359574679' => 
        array (
          0 => 'thesis_comment_form_1359574679_thesis_comment_form_title',
          1 => 'thesis_comment_form_1359574679_thesis_comment_form_cancel',
          2 => 'thesis_comment_form_1359574679_thesis_comment_form_name',
          3 => 'thesis_comment_form_1359574679_thesis_comment_form_email',
          4 => 'thesis_comment_form_1359574679_thesis_comment_form_url',
          5 => 'thesis_comment_form_1359574679_thesis_comment_form_comment',
          6 => 'thesis_comment_form_1359574679_thesis_comment_form_submit',
        ),
        'promo_box' => 
        array (
          0 => 'promo_box_main_promo_box',
        ),
      ),
    ),
  ),
);
	foreach ($all as $key => $data)
		update_option($key, (strpos($key, 'css') ? strip_tags($data) : $data));
}
wp_cache_flush();