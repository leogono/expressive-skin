<?php


/* Thesis Boxes by ThesisLove.Com, specically developed for the Expressive Skin by ThesisLove */
/* Developed & Designed By - Raaj Trambadia. */
/* Visit ThesisLove.Com/Support For Any Additional Help */

/* x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x- */

/* ====== [[ ThesisLove Logo Box ]] ====== */

class thesislove_logo_box extends thesis_box {
	protected function translate() {
		$this->title = __('ThesisLove → Logo Box', 'thesislove');
	}
	
	/* Box Options */
	
	protected function options() {
     global $thesis;
     return array(
	 'img' => array(
              'type' => 'text',
              'width' => 'full',
              'label' => 'Image URL',
              'tooltip' => __('Upload Your Logo Anywhere, And Paste In The URL Here', 'thesislove')),
	 'img_alt' => array(
              'type' => 'text',
              'width' => 'medium',
              'label' => __('Image ALT Text', 'diywp'),
              'tooltip' => __('Provide The ALT Text For Your Image (Usually The Site Title).', 'diywp')),
     'img_width' => array(
              'type' => 'text',
              'width' => 'tiny',
              'label' => __('Image Width', 'thesislove'),
			  'description' => __('*Height Is Automatically Adjusted Propotionately To The Width', 'thesislove'),
              'tooltip' => __('Provide A Custom Width For Your Logo (Else It Shall Stay In Its Original Size)', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
	 $img = !empty($this->options['img']) ? $this->options['img'] : '';
     $img_alt = !empty($this->options['img_alt']) ? $this->options['img_alt'] : '';
     $img_width = !empty($this->options['img_width']) ? $this->options['img_width'] : '';
	 $url = site_url();
	 

              ?>
            	<div class="thesislove-logo">
               <a href="<?php echo $url ?>"><img src="<?php echo $img ?>" width="<?php echo $img_width ?>" alt="<?php echo $img_alt ?>"/></a>
               </div>
                
                
                <?
              }
}

/* ====== [[ ThesisLove Author Box ]] ====== */

class thesislove_author_box extends thesis_box {
	protected function translate() {
		$this->title = __('ThesisLove → Author Box ', 'thesislove');
	}
	
	protected function options() { }
	
	public function html() {
		global $thesis;
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
	if (is_single())
{ ?>
<div id="thesislove-postauthor">
<div class="thesislove-author-info">
<?php echo get_avatar( get_the_author_id() , 75 ); ?>
<h4>About <?php the_author_posts_link(); ?></h4>
<?php the_author_description(); ?>
</div>
</div>
<?	}
	}
}
	
	
/* ====== [[ ThesisLove Feature Boxes ]] ====== */

class feature_boxes extends thesis_box {
	public $type = 'rotator';
	public $dependents = array('fb_item_1', 'fb_item_2', 'fb_item_3', 'fb_item_4');
	public $children = array('fb_item_1', 'fb_item_2', 'fb_item_3', 'fb_item_4');

	public function translate() {
		$this->title = $this->name = __('Feature Boxes → ThesisLove', 'thesislove');
	}
	
	public function options() {
		global $thesis;
		$options = $thesis->api->html_options();
		$options['class']['default'] = 'feature-boxes';
		return $options;
	}

	public function html($args = false) {
		global $thesis;
		$tab = str_repeat("\t", !empty($depth) ? $depth : 0);
		echo
			"<div" . (!empty($this->options['id']) ? ' id="' . trim($thesis->api->esc($this->options['id'])) . '"' : '') . ' class="feature-boxes '. (!empty($this->options['class']) ? trim($thesis->api->esc($this->options['class'])) : 'feature-boxes') . "\">\n".
			"<ul>",
			$this->rotator($depth + 1),
			"</ul>".
			"</div>\n";
	}
}

class fb_item_1 extends thesis_box {
	protected function translate() {
		$this->title = __('Feature Boxes Item 1', 'thesislove');
	}
	
	
	protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Top Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See The Help[?] for the same', 'thesislove')),
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 2-3 Words.', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Description To Be About A Couple Of Sentences', 'thesislove')),
	  'buttoncolor' => array(
              'type' => 'select',
			  'label' => __('Button Color', 'thesislove'),
			  'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
              'tooltip' => __('Select Your Button Color', 'thesislove')),
	  'buttontext' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Button Text', 'thesislove'),
            'tooltip' => __('Enter The Button Text', 'thesislove'),
            'placeholder' => __('e.g. Join Us', 'thesislove')),
	  'link' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Button Link', 'thesislove'),
            'tooltip' => __('Link Where The Button Will Point To', 'thesislove'),
            'placeholder' => __('Start with "http", please.', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '';
			$buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
			$buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : '';

/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		
		?>
<li>
<i class="<?php echo $icon ?>"></i>
<h2><?php echo $headline ?></h2>
<p><?php echo $description ?></p>
<a class="button <?php echo $buttoncolor ?>" href="<?php echo $link ?>"><?php echo $buttontext ?></a>
</li>
        <?
	}
}

class fb_item_2 extends thesis_box {
	protected function translate() {
		$this->title = __('Feature Boxes Item 2', 'thesislove');
	}
	
	
	protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See The Help[?] for the same', 'thesislove')),
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Description To Be About A Couple Of Sentences', 'thesislove')),
	  'buttoncolor' => array(
              'type' => 'select',
			  'label' => __('Button Color', 'thesislove'),
			  'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
              'tooltip' => __('Select Your Button Color', 'thesislove')),
	  'buttontext' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Button Text', 'thesislove'),
            'tooltip' => __('Enter The Button Text', 'thesislove'),
            'placeholder' => __('e.g. Join Us', 'thesislove')),
	  'link' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Button Link', 'thesislove'),
            'tooltip' => __('Link Where The Button Will Point To', 'thesislove'),
            'placeholder' => __('Start with "http", please.', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '';
			$buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
			$buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : '';

/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		
		?>
<li>
<i class="<?php echo $icon ?>"></i>
<h2><?php echo $headline ?></h2>
<p><?php echo $description ?></p>
<a class="button <?php echo $buttoncolor ?>" href="<?php echo $link ?>"><?php echo $buttontext ?></a>
</li>
        <?
	}
}

class fb_item_3 extends thesis_box {
	protected function translate() {
		$this->title = __('Feature Boxes Item 3', 'thesislove');
	}
	
	
	protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See The Help[?] for the same', 'thesislove')),
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Description To Be About A Couple Of Sentences', 'thesislove')),
	  'buttoncolor' => array(
              'type' => 'select',
			  'label' => __('Button Color', 'thesislove'),
			  'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
              'tooltip' => __('Select Your Button Color', 'thesislove')),
	  'buttontext' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Button Text', 'thesislove'),
            'tooltip' => __('Enter The Button Text', 'thesislove'),
            'placeholder' => __('e.g. Join Us', 'thesislove')),
	  'link' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Button Link', 'thesislove'),
            'tooltip' => __('Link Where The Button Will Point To', 'thesislove'),
            'placeholder' => __('Start with "http", please.', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '';
			$buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
			$buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : '';

/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		
		?>
<li>
<i class="<?php echo $icon ?>"></i>
<h2><?php echo $headline ?></h2>
<p><?php echo $description ?></p>
<a class="button <?php echo $buttoncolor ?>" href="<?php echo $link ?>"><?php echo $buttontext ?></a>
</li>
        <?
	}
}

class fb_item_4 extends thesis_box {
	protected function translate() {
		$this->title = __('Feature Boxes Item 4', 'thesislove');
	}
	
	
	protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See The Help[?] for the same', 'thesislove')),
	 'headline' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Description To Be About A Couple Of Sentences', 'thesislove')),
	  'buttoncolor' => array(
              'type' => 'select',
			  'label' => __('Button Color', 'thesislove'),
			  'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
              'tooltip' => __('Select Your Button Color', 'thesislove')),
	  'buttontext' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Button Text', 'thesislove'),
            'tooltip' => __('Enter The Button Text', 'thesislove'),
            'placeholder' => __('e.g. Join Us', 'thesislove')),
	  'link' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Button Link', 'thesislove'),
            'tooltip' => __('Link Where The Button Will Point To', 'thesislove'),
            'placeholder' => __('Start with "http", please.', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '';
			$buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
			$buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : '';

/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		
		?>
<li>
<i class="<?php echo $icon ?>"></i>
<h2><?php echo $headline ?></h2>
<p><?php echo $description ?></p>
<a class="button <?php echo $buttoncolor ?>" href="<?php echo $link ?>"><?php echo $buttontext ?></a>
</li>
        <?
	}
}


/* ====== [[ ThesisLove Floating Sharebar ]] ====== */


class sharebar extends thesis_box {
	protected function translate() {
		$this->title = __('Floating Sharebar → ThesisLove', 'thesislove');
	}

	
	/* Box Options */
	
	protected function options() {
     global $thesis;
     return array(
	 'display' => array(
				'type' => 'checkbox',
					'label' => __('Display Options', 'thesislove'),
					'options' => array(
						'twitter' => __('Tweet Button', 'thesislove'),
						'facebook' => __('Facebook Like Button', 'thesislove'),
						'plusone' => __('Google +1 Button', 'thesislove'),
						'linkedin' => __('LinkedIn Button', 'thesislove'),
						'stumble' => __('Stumble Upon Button', 'thesislove'),
						'scoopit' => __('ScoopIt Button', 'thesislove'),
						'buffer' => __('Buffer Button', 'thesislove')
					),
					'default' => array(
						'twitter' => true,
						'facebook' => true,
						'plusone' => true,
						'linkedin' => true,
						'stumble' => true,
						'scoopit' => false,
						'buffer' => false
					)
				),
	'twitter_name' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Twitter & Buffer Username (for the "via" mentions)', 'thesislove'),
            'tooltip' => __('Enter Your Twitter Username WITHOUT [@]', 'thesislove'),
            'placeholder' => __('', 'thesislove')));
	}

public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$twitter_name = !empty($this->options['twitter_name']) ? $this->options['twitter_name'] : '';
			
		/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Starting Of HTML */
		
						?>
							<div class="sharebar" >
							<div class="tweet_sharer">
                            <?php if( $options['display']['twitter'] ){ ?>
						<a rel="nofollow" href="http://twitter.com/share" class="twitter-share-button" data-via="<?php echo $twitter_name ?>" data-related="<?php echo $twitter_name ?>" data-count="vertical">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</div>
                            <?php } ?>
                            <div class="like_sharer">
                            <?php if( $options['display']['facebook'] ){ ?>
<iframe src="//www.facebook.com/plugins/like.php?href=<?php echo urlencode(get_permalink($post->ID)); ?>&amp;send=false&amp;layout=box_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=90" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:65px; height:65px;" allowTransparency="true"></iframe>
							</div>
                            <?php } ?>
                            <div class="gplus_sharer">
                            <?php if( $options['display']['plusone'] ){ ?>
                            <script type="text/javascript">
		(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/plusone.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  		})();
	</script>
    <g:plusone size="tall" href="<?php the_permalink(); ?>"></g:plusone>
							</div>
                            <?php } ?>
                            <div class="lin_sharer">
                            <?php if( $options['display']['linkedin'] ){ ?>
	<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/Share" data-counter="top"></script>
							</div>
                            <?php } ?>
                            <div class="stumble_sharer">
                            <?php if( $options['display']['stumble'] ){ ?>
<script src="http://www.stumbleupon.com/hostedbadge.php?s=5"></script>
							</div>
                            <?php } ?>
                            <div class="scoopit_sharer">
                            <?php if( $options['display']['scoopit'] ){ ?>
                            <a rel="nofllow" href="<?php echo get_permalink(); ?>" class="scoopit-button" scit-position="vertical">Scoop.it</a><script type="text/javascript" src="http://www.scoop.it/button/scit.js"></script>
                            </div>
                            <?php } ?>
                            <div class="buffer_sharer">
                            <?php if( $options['display']['buffer'] ){ ?>
                            <a rel="nofollow" href="http://bufferapp.com/add" class="buffer-add-button" data-url="<?php echo get_permalink(); ?>" data-count="vertical" data-via="<?php echo $twitter_name ?>" >Buffer</a><script type="text/javascript" src="http://static.bufferapp.com/js/button.js"></script>
                            </div>
                            <?php } ?>
                            </div>
                      <?php
							}
						}

/* ====== [[ ThesisLove Promo Box ]] ====== */

class promo_box extends thesis_box {
	public $type = 'rotator';
	public $dependents = array('main_promo_box');
	public $children = array('main_promo_box');
	
	
	public function translate() {
		$this->title = __('Promo Box → ThesisLove', 'thesislove');
	}
	
	public function options() {
		global $thesis;
		$options = $thesis->api->html_options();
		$options['class']['default'] = 'promo-box';
		return $options;
	}
	
	public function html($args = false) {
		global $thesis;
		$tab = str_repeat("\t", !empty($depth) ? $depth : 0);
		echo
			"<div" . (!empty($this->options['id']) ? ' id="' . trim($thesis->api->esc($this->options['id'])) . '"' : '') . ' class="promo-box '. (!empty($this->options['class']) ? trim($thesis->api->esc($this->options['class'])) : 'promo-box') . "\">\n".
			$this->rotator($depth + 2),
			"</div>\n";
	}
}
	
class main_promo_box extends thesis_box {
	protected function translate() {
		$this->title = __('Main Promo Box', 'thesislove');
	}
	
	/* Box Options */
	
	protected function options() {
     global $thesis;
     return array(
	 'background' => array(
              'type' => 'text',
              'width' => 'medium',
              'label' => 'Background Color',
              'tooltip' => __('Enter The Background Color In #Hex Format e.g. #990000', 'thesislove')),
	 'headline' => array(
              'type' => 'text',
              'width' => 'full',
              'label' => __('Headline Text', 'diywp'),
              'tooltip' => __('Enter The Main Text To Be Displayed Inside The Promo Box', 'thesislove')),
	 'fontsize' => array(
              'type' => 'text',
              'width' => 'tiny',
              'label' => __('Font Size', 'diywp'),
			  'placeholder' => __('35px', 'thesislove'),
              'tooltip' => __('Enter The Font Size For The Text In The Promo Box', 'thesislove')),
     'buttontext' => array(
              'type' => 'text',
              'width' => 'medium',
              'label' => __('Button Text', 'thesislove'),
              'tooltip' => __('Enter The Text To Be Displayed As A Button', 'thesislove')),
	 'buttonlink' => array(
              'type' => 'text',
              'width' => 'medium',
              'label' => __('Button Link', 'thesislove'),
              'tooltip' => __('Enter The Link Where The Button Will Redirect To', 'thesislove')),
     'buttoncolor' => array(
              'type' => 'select',
			  'label' => __('Button Color', 'thesislove'),
			  'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
              'tooltip' => __('Select Your Button Color', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
	 $background = !empty($this->options['background']) ? $this->options['background'] : '#00b58b';
     $headline = !empty($this->options['headline']) ? $this->options['headline'] : 'Your Promo Box Custom Headline';
	 $fontsize = !empty($this->options['fontsize']) ? $this->options['fontsize'] : '35px';
	 $buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : 'Button Text';
	 $buttonlink = !empty($this->options['buttonlink']) ? $this->options['buttonlink'] : '#';
	 $buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
	 
	 /* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
	 
	 
		
		/* Box on the frontend */
		
		?>
		<div style="background:<?php echo $background ?>; color: #fff; font-size:<?php echo $fontsize ?>; padding: 8px 0px; text-align: center;">
        <div class="page">
        <p> <? echo $headline ?> <a class="button <?php echo $buttoncolor ?>" href="<?php echo $buttonlin ?>"><?php echo $buttontext ?></a> </p>
        </div>
        </div>
        <?
	}
}    


/* ====== [[ ThesisLove Peppy Boxes ]] ====== */
     
class peppy_boxes extends thesis_box {
	public $type = 'rotator';
	public $dependents = array('peppy_box_1', 'peppy_box_2', 'peppy_box_3', 'peppy_box_4');
	public $children = array('peppy_box_1', 'peppy_box_2', 'peppy_box_3', 'peppy_box_4');

	public function translate() {
		$this->title = $this->name = __('Peppy Boxes → ThesisLove', 'thesislove');
	}
	

public function options() {
		global $thesis;
		$options = $thesis->api->html_options();
		$options['class']['default'] = 'peppy-boxes';
		return $options;
	}

	public function html($args = false) {
		global $thesis;
		$tab = str_repeat("\t", !empty($depth) ? $depth : 0);
		echo
			"<div" . (!empty($this->options['id']) ? ' id="' . trim($thesis->api->esc($this->options['id'])) . '"' : '') . ' class="peppy-boxes '. (!empty($this->options['class']) ? trim($thesis->api->esc($this->options['class'])) : 'peppy-boxes') . "\">\n".
			"<ul>",
			$this->rotator($depth + 1),
			"</ul>".
			"</div>\n";
	}
}

class peppy_box_1 extends thesis_box {
	protected function translate() {
		$this->title = __('Peppy Box 1', 'thesislove');
	}
	
	protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See Help[?]', 'thesislove')),
	'link' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Link', 'thesislove'),
            'tooltip' => __('Enter The Link Where The Box Will Lead To Upon Clicking', 'thesislove'),
            'placeholder' => __('Start with "http".', 'thesislove')),
	  
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Descripton To Be Short As Well', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '#';
			 
			
			/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		?>
    <li>
    	
        <a href="<?php echo $link ?>">
        <i class="<?php echo $icon ?>"></i>
            <div class="peppy-content">
                <h2 class="peppy-content-main"><?php echo $headline ?></h2>
                <h3 class="peppy-content-sub"><?php echo $description ?></h3>
            </div>
        </a>
    </li>
    <?
	}
}


class peppy_box_2 extends thesis_box {
	protected function translate() {
		$this->title = __('Peppy Box 2', 'thesislove');
	}
	
	
protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See Help[?]', 'thesislove')),
	 'link' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Link', 'thesislove'),
            'tooltip' => __('Enter The Link Where The Box Will Lead To Upon Clicking', 'thesislove'),
            'placeholder' => __('Start with "http".', 'thesislove')),
	  		
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Descripton To Be Short As Well', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '#';
			 
			
			/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		?>
    <li>
    	
        <a href="<?php echo $link ?>">
        <i class="<?php echo $icon ?>"></i>
            <div class="peppy-content">
                <h2 class="peppy-content-main"><?php echo $headline ?></h2>
                <h3 class="peppy-content-sub"><?php echo $description ?></h3>
            </div>
        </a>
    </li>
    <?
	}
}

class peppy_box_3 extends thesis_box {
	protected function translate() {
		$this->title = __('Peppy Box 3', 'thesislove');
	}
	
	
protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See Help[?]', 'thesislove')),
	 'link' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Link', 'thesislove'),
            'tooltip' => __('Enter The Link Where The Box Will Lead To Upon Clicking', 'thesislove'),
            'placeholder' => __('Start with "http".', 'thesislove')),
	  
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Descripton To Be Short As Well', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '#';
			 
			
			/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		?>
    <li>
    	
        <a href="<?php echo $link ?>">
        <i class="<?php echo $icon ?>"></i>
            <div class="peppy-content">
                <h2 class="peppy-content-main"><?php echo $headline ?></h2>
                <h3 class="peppy-content-sub"><?php echo $description ?></h3>
            </div>
        </a>
    </li>
    <?
	}
}


class peppy_box_4 extends thesis_box {
	protected function translate() {
		$this->title = __('Peppy Box 4', 'thesislove');
	}
	
	
protected function options() {
     global $thesis;
     return array(
     'icon' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Icon', 'thesislove'),
            'tooltip' => __('Visit <a href="http://thesislove.com/web-icons">ThesisLove Icons</a> and choose your preferred Icon.', 'thesislove'),
            'placeholder' => __('See Help[?]', 'thesislove')),
	'link' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Link', 'thesislove'),
            'tooltip' => __('Enter The Link Where The Box Will Lead To Upon Clicking', 'thesislove'),
            'placeholder' => __('Start with "http".', 'thesislove')),		
	  
	 'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Headline', 'thesislove'),
            'tooltip' => __('Keep It Short - About 1-2 Words"', 'thesislove')),
	  'description' => array(
            'type' => 'textarea',
            'rows' => 2,
            'label' => __('Description', 'thesislove'),
            'tooltip' => __('Keep Your Descripton To Be Short As Well', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$icon = !empty($this->options['icon']) ? $this->options['icon'] : '';
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : '';
			$description = !empty($this->options['description']) ? $this->options['description'] : '';
			$link = !empty($this->options['link']) ? $this->options['link'] : '#';
			 
			
			/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		?>
    <li>
    	
        <a href="<?php echo $link ?>">
        <i class="<?php echo $icon ?>"></i>
            <div class="peppy-content">
                <h2 class="peppy-content-main"><?php echo $headline ?></h2>
                <h3 class="peppy-content-sub"><?php echo $description ?></h3>
            </div>
        </a>
    </li>
    <?
	}
}


/* ====== [[ ThesisLove Single-Post Optin (Aweber) Box ]] ====== */

class single_optin extends thesis_box {
	protected function translate() {
		$this->title = __('Single-Post Aweber Optin Form → ThesisLove', 'thesislove');
	}
	
	
	protected function options() {
     global $thesis;
     return array(
     'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Your Optin Form Headline', 'thesislove'),
            'tooltip' => __('Enter The Main Headline Of The Optin Form', 'thesislove'),
            'placeholder' => __('E.g. Free Traffic Generation eBook!', 'thesislove')),
	 'sub_headline' => array(
            'type' => 'textarea',
            'rows' => 3,
            'label' => __('Optin Form Description', 'thesislove'),
            'tooltip' => __('Enter The Description For The Optin Form', 'thesislove'),
            'placeholder' => __('E.g. Enter Your Details To Download The eBook Immediately! Hit The Button Once You Have Entered The Details', 'thesislove')),
	 'listname' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Your Aweber List Name', 'thesislove'),
            'tooltip' => __('Enter Your Aweber List Name', 'thesislove')),
	 'webtracking' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Your Aweber Web-Tracking ID', 'thesislove'),
            'tooltip' => __('Enter The Web-Tracking ID for your Aweber List', 'thesislove')),
	 'emailinput' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Email Input Text', 'thesislove'),
            'tooltip' => __('Enter The Email Input Text', 'thesislove'),
            'placeholder' => __('E.g. Enter Your Email', 'thesislove')),
	 'nameinput' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Name Input Text', 'thesislove'),
            'tooltip' => __('Enter The Name Input Text', 'thesislove'),
            'placeholder' => __('E.g. Enter Your Name', 'thesislove')),
     'buttontext' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Submit Button Text', 'thesislove'),
            'tooltip' => __('The Text You Want To Be Displayed In the Submit Box', 'thesislove'),
            'placeholder' => __('e.g. Submit', 'thesislove')),
	 'buttoncolor' => array(
				'type' => 'select',
				'label' => __('Button Color', 'thesislove'),
				'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
				'tooltip' => __('Choose The Button Color', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : 'e.g. Your Custom Headline';
			$sub_headline = !empty($this->options['sub_headline']) ? $this->options['sub_headline'] : 'e.g Your Custom Description Goes Here...';
			$listname = !empty($this->options['listname']) ? $this->options['listname'] : 'thesislove';
			$webtracking = !empty($this->options['webtracking']) ? $this->options['webtracking'] : '';
			$emailinput = !empty($this->options['emailinput']) ? $this->options['emailinput'] : 'Your Email Address';
			$nameinput = !empty($this->options['nameinput']) ? $this->options['nameinput'] : 'Your Name';
			$buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : 'e.g. Join Now!';
			$buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
			
			/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		?>
        <div class="single-email">
<h1><?php echo $headline ?></h1>
<h4><?php echo $sub_headline ?></h4>
<form method="post" action="http://www.aweber.com/scripts/addlead.pl"  >
<div style="display: none;">
		<input type="hidden" name="meta_web_form_id" value="" /><br />
		<input type="hidden" name="meta_split_id" value="" /><br />
		<input type="hidden" name="listname" value="<? echo $listname ?>" /><br />
		<input type="hidden" name="redirect" value="http://www.aweber.com/thankyou-coi.htm?m=text" id="redirect_8fb78b8c1d56730b21b4d6bb5dd321b3" /><br />
		<input type="hidden" name="meta_adtracking" value="<? echo $web_tracking ?>" /><br />
		<input type="hidden" name="meta_message" value="1" /><br />
		<input type="hidden" name="meta_required" value="name,email" /><br />
		<input type="hidden" name="meta_tooltip" value="" />
	</div>
<p><input type="text" name="name" class="name" placeholder="<? echo $nameinput ?>" tabindex="600" /></p>
<p><input class="email" type="email" name="email" placeholder="<? echo $emailinput ?>" tabindex="601" /></p>
<p><input class="button <? echo $buttoncolor ?>" name="submit" type="submit" value="<? echo $buttontext ?>" tabindex="602" /></p>
</form>
</div>
<?
	}
}


/* ====== [[ ThesisLove Single-Post Optin (Aweber) Box ]] ====== */

class single_optin_mailchimp extends thesis_box {
	protected function translate() {
		$this->title = __('Single-Post Mailchimp Optin Form → ThesisLove', 'thesislove');
	}
	
	
	protected function options() {
     global $thesis;
     return array(
     'headline' => array(
            'type' => 'text',
            'width' => 'full',
            'label' => __('Your Optin Form Headline', 'thesislove'),
            'tooltip' => __('Enter The Main Headline Of The Optin Form', 'thesislove'),
            'placeholder' => __('E.g. Free Traffic Generation eBook!', 'thesislove')),
	 'sub_headline' => array(
            'type' => 'textarea',
            'rows' => 3,
            'label' => __('Optin Form Description', 'thesislove'),
            'tooltip' => __('Enter The Description For The Optin Form', 'thesislove'),
            'placeholder' => __('E.g. Enter Your Details To Download The eBook Immediately! Hit The Button Once You Have Entered The Details', 'thesislove')),
	 'listname' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Your Mailchimp Form URL', 'thesislove'),
            'tooltip' => __('Enter Your Mailchimp Form URL', 'thesislove')),
	 'emailinput' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Email Input Text', 'thesislove'),
            'tooltip' => __('Enter The Email Input Text', 'thesislove'),
            'placeholder' => __('E.g. Enter Your Email', 'thesislove')),
	 'nameinput' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Name Input Text', 'thesislove'),
            'tooltip' => __('Enter The Name Input Text', 'thesislove'),
            'placeholder' => __('E.g. Enter Your Name', 'thesislove')),
     'buttontext' => array(
            'type' => 'text',
            'width' => 'medium',
            'label' => __('Submit Button Text', 'thesislove'),
            'tooltip' => __('The Text You Want To Be Displayed In the Submit Box', 'thesislove'),
            'placeholder' => __('e.g. Submit', 'thesislove')),
	 'buttoncolor' => array(
				'type' => 'select',
				'label' => __('Button Color', 'thesislove'),
				'options' => array('red' => 'Red', 'blue' => 'Blue', 'orange' => 'Orange', 'green' => 'Green', 'black' => 'Black', 'brown' => 'Brown', 'rosy' => 'Rosy', 'cyan' => 'Cyan', 'purple' => 'Purple', 'pink' => 'Pink', 'yellow' => 'Yellow'),
				'tooltip' => __('Choose The Button Color', 'thesislove')));
	}
	
	public function html($args = false) {
		    global $thesis;
            extract($args = is_array($args) ? $args : array());
            $tab = str_repeat("\t", !empty($depth) ? $depth : 0);
			
			$headline = !empty($this->options['headline']) ? $this->options['headline'] : 'e.g. Your Custom Headline';
			$sub_headline = !empty($this->options['sub_headline']) ? $this->options['sub_headline'] : 'e.g Your Custom Description Goes Here...';
			$listname = !empty($this->options['listname']) ? $this->options['listname'] : 'thesislove';
			$emailinput = !empty($this->options['emailinput']) ? $this->options['emailinput'] : 'Your Email Address';
			$nameinput = !empty($this->options['nameinput']) ? $this->options['nameinput'] : 'Your Name';
			$buttontext = !empty($this->options['buttontext']) ? $this->options['buttontext'] : 'Join Now!';
			$buttoncolor = !empty($this->options['buttoncolor']) ? $this->options['buttoncolor'] : 'black';
			
			/* Get Thesis Options */
		
		$options = $thesis->api->get_options($this->_options(), $this->options);
		
		/* Start Of HTML */
		?>
        <div class="single-email">
<h1><?php echo $headline ?></h1>
<h4><?php echo $sub_headline ?></h4>
<form action="<?php echo $listname ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<p><input type="text" value="" name="FNAME" class="name" id="mce-NAME" placeholder="<?php echo $nameinput ?>" required></p>
	<p><input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="<?php echo $emailinput ?>" required></p>
	<p><input type="submit" value="<?php echo $buttontext ?>" name="subscribe" id="mc-embedded-subscribe" class="button <?php echo $buttoncolor ?>"></p>
</form>
</div>
<?
	}
}



/* ====== [[ ThesisLove Attribution Box ]] ====== */


class thesislove_att extends thesis_box {
	public function translate() {
		$this->title = __('ThesisLove Attribution', 'thesislove');
	}

	public function options() {
		return array(
			'link' => array(
				'type' => 'text',
				'width' => 'full',
				'label' => __('Affiliate Link', 'thesislove'),
				'tooltip' => __('Enter Your ThesisLove Affiliate Link Here. You can find your unique links at the <a href="http://thesislove.com/affiliates">ThesisLove Affiliates</a> page', 'thesislove')));
	}

	public function html($args = false) {
		global $thesis;
		$tab = str_repeat("\t", $depth = !empty($depth) ? $depth : 0);
		
		$link = !empty($this->options['link']) ? $this->options['link'] : 'http://thesislove.com';
		
			?>
            <div class="tl_att">
            <p>Site Designed By <a href="<?php echo $link ?>">ThesisLove</a>.</p>
            </div>
            <?
			
	}
}