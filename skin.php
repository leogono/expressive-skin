<?php

/*
Name: Expressive
Author: Raaj Trambadia (Thesislove.Com)
Version: 1.1
Description: It's Time To Be Expressive And Show The World What You've Got - Time To Be Yourself. With Expressive, Speak Your Heart or Mind Out!
Class: expressive
*/

class expressive extends thesis_skin {
	
	/* Add The Functions */
	
	function construct() {
		wp_enqueue_style('vocation-icons', THESIS_USER_SKIN_URL . "/thesislove/styles/thesislove-icons.css");
		add_filter('thesis_css', array($this, 'filter_css'));
		add_action('wp_head', array($this, 'google_fonts'));
		add_action('wp_head', array($this, 'responsive_menu'));
		add_filter('thesis_editor_box_options', '__return_true');
		require_once(dirname(__FILE__) . '/thesislove/plugins-notice/plugin-installation.php');
	}
	
	public function filter_css($css) {
    $my_css = file_get_contents(THESIS_USER_SKIN_URL . '/thesislove/styles/expressive.css');
    return $css. $my_css;
}

	public function meta_viewport() {
	return 'width=device-width';
}


	protected function design() {
		global $thesis;
		$css = $thesis->api->css->options;
		$fsc = $nav = $thesis->api->css->font_size_color();
		unset($nav['color']);
		return array(
			'colors' => $this->color_scheme(array(
				'id' => 'colors',
				'colors' => array(
					'dark' => __('Primary Dark Color', $this->_class),
					'primary' => __('Links', $this->_class),
					'hover' => __('Hovered Links (MouseOver)', $this->_class)),
				'default' => array(
					'dark' => '0a0a0a',
					'primary' => '009900',
					'hover' => '005500'),
				'scale' => array(
						'primary' => '888888',
						'hover' => '565656'))),
			'layout' => array(
				'type' => 'group',
				'label' => __('Site Layout Options', $this->_class),
				'fields' => array(
					'content' => array(
						'type' => 'text',
						'width' => 'tiny',
						'label' => __('Content Width', $this->_class),
						'default' => ('700'),
						'tooltip' => __('The default content column width is 700px.', $this->_class),
						'description' => 'px &#8594; Default is 700px.'),
					'sidebar' => array(
						'type' => 'text',
						'width' => 'tiny',
						'label' => __('Sidebar Width', $this->_class),
						'default' => ('340'),
						'tooltip' => __('The default sidebar column width is 340px.', $this->_class),
						'description' => 'px &#8594; Default is 340px.'))),						
			'generalfonts' => array(
				'type' => 'group',
				'label' => __('General Font Options', $this->_class),
				'fields' => array(
					'font' => array_merge(
						$css['font']['fields']['font-family'], array(
						'label' => __('Font Family'),
						'default' => 'gill_sans')),
						'tooltip' => __('Choose The Primary Font For The Site', $this->_class),
						'size' => $css['font']['fields']['font-size'],
						'default' => '16')),
			 'titlefonts' => array(
				'type' => 'group',
				'label' => __('Title Font Options (Posts & Widget Titles)', $this->_class),
				'fields' => array(
					'titles' => array(
						'type' => 'text',
						'width' => 'medium',
						'label' => __('Font Family', $this->_class),
						'default' => ('Signika'),
						'tooltip' => __('The default Font for the titles is "Signika"', $this->_class),
						'description' => ' &#8594; Default is Signika (a <a href="http://www.google.com/fonts/specimen/Signika">Google WebFont</a>)'),
					'titlessize' => array(
						'type' => 'text',
						'width' => 'tiny',
						'label' => __('Font Size', $this->_class),
						'default' => ('26'),
						'tooltip' => __('The default font size for the titles is 26px.', $this->_class),
						'description' => 'px &#8594; Default is 26px.'))),
			 'sitebg' => array(
				'type' => 'group',
				'label' => __('Site Background Options', $this->_class),
				'fields' => array(
					'bgimg' => array(
						'type' => 'text',
						'width' => 'full',
						'default' => ('url(images/b-pattern.gif)'),
						'label' => __('Site Background Image', $this->_class),
						'tooltip' => __('Enter The URL Of The Background Image. By default, the image shall be the one seen at demo.', $this->_class),
						'description' => 'Enter The Image Path In This Way → url(images/yourimageURLorPATH) IF the image is located in the Skin Editor. If you have the full URL to the image, enter it in this way → url("fullimageurl.png") - yes, use the quotes!.'),
					'ftimg' => array(
						'type' => 'text',
						'width' => 'full',
						'default' => ('url(images/dark_brick_wall.png)'),
						'label' => __('Footer Background Image', $this->_class),
						'tooltip' => __('Enter The URL Of The Background Image. By default, the image shall be the one seen at demo.', $this->_class),
						'description' => 'Enter The Image Path In This Way → url(images/yourimageURLorPATH) IF the image is located in the Skin Editor. If you have the full URL to the image, enter it in this way → url("fullimageurl.png") - yes, use the quotes!. '))));
					
			
	}
	
	
    
	public function css_variables() {
		global $thesis;
		$px['content'] = !empty($this->design['content']) && is_numeric($this->design['content']) ?
			abs($this->design['content']) : 700;
		$px['sidebar'] = !empty($this->design['sidebar']) && is_numeric($this->design['sidebar']) ?
			abs($this->design['sidebar']) : 340;
		$px['width'] = $px['content'] + $px['sidebar'] + 20;
		$px['size'] = !empty($this->design['size']) && is_numeric($this->design['size']) ?
			abs($this->design['size']) : 16;
		$px['titlessize'] = !empty($this->design['titlessize']) && is_numeric($this->design['titlessize']) ?
			abs($this->design['titlessize']) : 26; 
		$vars['bgimg'] = !empty($this->design['bgimg']) ? $this->design['bgimg'] : 'url(images/b-pattern.gif)';
		$vars['ftimg'] = !empty($this->design['ftimg']) ? $this->design['ftimg'] : 'url(images/dark_brick_wall.png)';
		$vars['titles'] = !empty($this->design['titles']) ? $this->design['titles'] : 'Signika; font-weight: 600';
		$vars['font'] = $thesis->api->fonts->family($font = !empty($this->design['font']) ? $this->design['font'] : 'gill_sans');
		$vars = is_array($px) ? array_merge($vars, $thesis->api->css->unit($px)) : $vars;
		foreach (array('dark', 'primary', 'hover') as $color)
			$vars[$color] = !empty($this->design[$color]) ? $thesis->api->colors->css($this->design[$color]) : false;
		// Determine multi-use color variables
		foreach (array('title', 'headline', 'subhead') as $name)
			$vars["{$name}_color"] = !empty($this->design[$name]['color']) ?
				$thesis->api->colors->css($this->design[$name]['color']) : (!empty($vars['dark']) ? $vars['dark'] : false);
		return array_filter($vars);
		
		
		
		
		
	}
	
	
		
	function google_fonts() {
echo "<link href='http://fonts.googleapis.com/css?family=Signika:400,700,600' rel='stylesheet' type='text/css'>";
	}
	
	function responsive_menu() {
	echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>";
	
	 echo "<script>
		$(function() {
			var pull 		= $('#pull');
				menu 		= $('nav ul');
				menuHeight	= menu.height();

			$(pull).on('click', function(e) {
				e.preventDefault();
				menu.slideToggle();
			});

			$(window).resize(function(){
        		var w = $(window).width();
        		if(w > 320 && menu.is(':hidden')) {
        			menu.removeAttr('style');
        		}
    		});
		});
	</script>";
	}
	
	public function boxes() {
		return array(
			'thesislove_logo_box',
			'thesislove_author_box',
			'feature_boxes',
			'sharebar',
			'page_nav',
			'promo_box',
			'peppy_boxes',
			'thesislove_att',
			'single_optin_mailchimp',
			'single_optin');
	}
	
	
}

include('thesislove/functions/widgets.php');
include('thesislove/functions/img.php');